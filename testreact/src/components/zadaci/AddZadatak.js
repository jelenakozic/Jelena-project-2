import React from 'react';
import ZadatakAxios from '../../apis/ZadatakAxios';

import {Form, Button} from 'react-bootstrap';

class AddZadatak extends React.Component {

    constructor(props){
        super(props);

        let zadatak = {
            ime: "",
            zaduzeni: "",
            bodovi: 0,
            pocetakZadatka: "",
            sprint: null,
            stanje: null
        }

        this.state = {zadatak: zadatak, sprintovi: [], stanja: []  };
    }

    componentDidMount(){
        this.getStanja();
        this.getSprintovi();
        console.log("test2");
    }

    // TODO: Dobaviti filmove
    async getStanja(){
        try{
            let result = await ZadatakAxios.get("/stanja");
            let stanja = result.data;
            this.setState({stanja: stanja});
            console.log("test1");
        }catch(error){
            console.log(error);
            alert("Couldn't fetch movies");
        }
    }

    async getSprintovi(){
        try{
            let result = await ZadatakAxios.get("/sprintovi");
            let sprintovi = result.data;
            this.setState({sprintovi: sprintovi});
            console.log("test1");
        }catch(error){
            console.log(error);
            alert("Couldn't fetch movies");
        }
    }

    async create(e){
        e.preventDefault();

        try{

            
           
            let zadatak = this.state.zadatak;
            let zadatakDTO = {
                pocetakZadatka: zadatak.pocetakZadatka,
                sprint: zadatak.sprint,
                ime: zadatak.ime,
                zaduzeni: zadatak.zaduzeni,
                bodovi: zadatak.bodovi,
                stanje: zadatak.stanje
            }

            let response = await ZadatakAxios.post("/zadaci", zadatakDTO);
            this.props.history.push("/zadaci");
        }catch(error){
            alert("Greska");
        }
    }

    valueInputChanged(e) {
        let input = e.target;
    
        let name = input.name;
        let value = input.value;
    
        let zadatak = this.state.zadatak;
        zadatak[name] = value;
    
        this.setState({ zadatak: zadatak });
      }

    // TODO: Rukovati prihvatom vrednosti na promenu
    sprintSelectionChanged(e){
        // console.log(e);

        let sprintId = e.target.value;
        let sprint = this.state.sprintovi.find((sprint) => sprint.id == sprintId);

        let zadatak = this.state.zadatak;
        zadatak.sprint = sprint;

        this.setState({zadatak: zadatak});
    }

    stanjeSelectionChanged(e){
        // console.log(e);

        let stanjeId = e.target.value;
        let stanje = this.state.stanja.find((stanje) => stanje.id == stanjeId);

        let zadatak = this.state.zadatak;
        zadatak.stanje = stanje;

        this.setState({zadatak: zadatak});
    }

    // TODO: Omogućiti odabir filma za projekciju
    render(){
        return (
            <div>
                <h1>Add Zadatak</h1>

                <Form>
                    <Form.Group>
                        <Form.Label htmlFor="ime">Ime</Form.Label>
                        <Form.Control id="ime" name="ime" onChange={(e)=>this.valueInputChanged(e)}/>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label htmlFor="zaduzeni">Zaduzeni</Form.Label>
                        <Form.Control id="zaduzeni" name="zaduzeni" onChange={(e)=>this.valueInputChanged(e)}/>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label id="bodovi">Bodovi</Form.Label>
                        <Form.Control type="number" id="bodovi" name="bodovi" onChange={(e)=>this.valueInputChanged(e)}/>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label htmlFor="pocetakZadatka">Pocetak zadatka</Form.Label>
                        <Form.Control   id="pocetakZadatka" name="pocetakZadatka" onChange={(e)=>this.valueInputChanged(e)}/>
                    </Form.Group>
                    
                    <Form.Group>
                        <Form.Label htmlFor="sprint">Sprint</Form.Label >


                    </Form.Group>
                    
                    <Form.Control as="select" id="sprint" onChange={event => this.sprintSelectionChanged(event)}>
                        <option></option>
                        {
                            this.state.sprintovi.map((sprint) => {
                                return (
                                    <option key={sprint.id} value={sprint.id}>{sprint.ime}</option>
                                )
                            })
                        }
                    </Form.Control><br/>
                    <Form.Control as="select" id="stanje" onChange={event => this.stanjeSelectionChanged(event)}>
                        <option></option>
                        {
                            this.state.stanja.map((stanje) => {
                                return (
                                    <option key={stanje.id} value={stanje.id}>{stanje.ime}</option>
                                )
                            })
                        }
                    </Form.Control><br/>

                    <Button variant="success" onClick={(event)=>{this.create(event);}}>Add</Button>
                </Form>
            </div>
        )
    }
}

export default AddZadatak;