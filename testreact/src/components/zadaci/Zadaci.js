import React from 'react';
import ZadatakAxios from '../../apis/ZadatakAxios';

// import './../../index.css';

import {Button, Table, Form} from 'react-bootstrap';


class Zadaci extends React.Component {

    constructor(props) {
        super(props);
    
        let params = {
          sprintId: "",
          zadName: "",
        };
    
        this.state = { zadaci: [], sprintovi: [], stanja: [],  params: params, pageNum: 0, totalPages: 1};
      }
    
      componentDidMount() {
        this.getZadaci();
        this.getSprintovi();
        this.getStanja();
      }
    
      async getZadaci() {
        try {
    
          let config = {params:{}};
          if(this.state.params.sprintId != ""){
            config.params.sprintId = this.state.params.sprintId;
          }
          if(this.state.params.zadName != ""){
            config.params.ime = this.state.params.zadName;
          }
    
          config.params.pageNum = this.state.pageNum;
    
          let result = await ZadatakAxios.get("/zadaci", config);
          this.setState({ zadaci: result.data, totalPages: result.headers["total-pages"]});
        } catch (error) {
          console.log(error);
        }
      }
    
      async getSprintovi(){
        try{
            let result = await ZadatakAxios.get("/sprintovi");
            let sprintovi = result.data;
            this.setState({sprintovi: sprintovi});
        }catch(error){
            console.log(error);
            alert("Couldn't fetch sprintovi");
        }
    }
    async getStanja(){
        try{
            let result = await ZadatakAxios.get("/stanja");
            let stanja = result.data;
            this.setState({stanja: stanja});
        }catch(error){
            console.log(error);
            alert("Couldn't fetch stanja");
        }
    }
    
    deleteFromState(zadatakId) {
      var zadaci = this.state.zadaci;
      zadaci.forEach((element, index) => {
          if (element.id === zadatakId) {
              zadaci.splice(index, 1);
              this.setState({zadaci: zadaci});
          }
      });
  }
  
  delete(zadatakId) {
      ZadatakAxios.delete('/zadaci/' + zadatakId)
      .then(res => {
          // handle success
          console.log(res);
          alert('Zadatak was deleted successfully!');
          // this.deleteFromState(zadatakId); // ili refresh page-a window.location.reload();
          this.getZadaci();
      })
      .catch(error => {
          // handle error
          console.log(error);
          alert('Error occured please try again!');
       });
  }
      goToAdd() {
        this.props.history.push("/zadaci/add");
      }
    
      valueInputChanged(e){
        let control = e.target;
    
        let name = control.name;
        let value = control.value;
    
        let params = this.state.params;
        params[name] = value;
    
        this.setState({params: params});
      }
    
      doSearch(e){
        e.preventDefault();
    
        this.setState({pageNum: 0}, ()=>{this.getZadaci();});
      }
    
      changePage(direction){
        let pageNum = this.state.pageNum;
        pageNum = pageNum + direction;
    
        this.setState({pageNum: pageNum}, ()=>{this.getZadaci()});
      }
      goToEdit(zadatakId) {
        this.props.history.push('/zadaci/edit/'+ zadatakId); 
    }
    changeStanje(zadatakId) {
      ZadatakAxios.put('/zadaci/' + zadatakId + '/promenaStanja')
        .then(res => {
            // handle success
            console.log('ZADATAK:')
            console.log(res);
            this.props.history.push('/zadaci');
            this.getZadaci();
        })
        .catch(error => {
            // handle error
            console.log(error);
            alert('Error occured please try again!');
         });
    }
    
      render() {
        return (
          <div>
            <h1>Zadaci</h1>
    
            <Form>
              <Form.Group>
                <Form.Label>Sprintovi</Form.Label>
                <Form.Control onChange={(event)=>this.valueInputChanged(event)} name="sprintId" as="select">
                  <option value=""></option>
                  {
                    this.state.sprintovi.map((sprint) => {
                      return <option key={sprint.id} value={sprint.id}>{sprint.ime}</option>
                    })
                  }
                </Form.Control>
              </Form.Group>
              <Form.Group>
                <Form.Label>Ime zadatka</Form.Label>
                <Form.Control onChange={(e)=>{this.valueInputChanged(e);}} name="zadName"></Form.Control>
              </Form.Group>
              <Button style={{marginBottom:"10px"}} onClick={(e)=>this.doSearch(e)}>Search</Button>
            </Form>
    
            <div>
              <Button onClick={() => this.goToAdd()}>
                Add
              </Button>
              <br />
    
              <br/>
              <br/>
    
              <Button disabled={this.state.pageNum==0} onClick={()=>this.changePage(-1)}>Previous</Button>
              <Button disabled={this.state.pageNum == this.state.totalPages - 1} onClick={()=>this.changePage(1)}>Next</Button>
              <Table striped id="movies-table">
                <thead className="thead-dark">
                  <tr>
                    <th>Sprint</th>
                    <th>Stanje</th>
                    <th>Naziv</th>
                    <th>Zaduzeni</th>
                    <th>Bodovi</th>
                    <th>Pocetak zadatka</th>
                    <th></th>
                    <th></th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                  {this.state.zadaci.map((zadatak) => {
                    return (
                      <tr key={zadatak.id}>
                        <td>{zadatak.sprint.ime}</td>
                        <td>{zadatak.stanje.ime}</td>
                        <td>{zadatak.ime}</td>
                        <td>{zadatak.zaduzeni}</td>
                        <td>{zadatak.bodovi}</td>
                        <th>{zadatak.pocetakZadatka}</th>
                        <td><Button  onClick={() => this.goToEdit(zadatak.id)}>Edit</Button></td>
                        <td><Button  onClick={() => this.delete(zadatak.id)}>Delete</Button></td>
                        <td><Button  onClick={() => this.changeStanje(zadatak.id)}>Promena stanja</Button></td>
                      
                      </tr>
                    );
                  })}
                </tbody>
              </Table>
            </div>
          </div>
        );
      }
    }
    
    export default Zadaci;
    