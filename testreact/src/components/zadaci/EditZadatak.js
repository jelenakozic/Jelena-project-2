import React from 'react';
import ZadatakAxios from '../../apis/ZadatakAxios';

import {Form, Button} from 'react-bootstrap';

class EditZadatak extends React.Component {

    constructor(props) {
        super(props);

        let zadatak = {
            ime: "",
            zaduzeni: "",
            bodovi: 0,
            pocetakZadatka: "",
            sprint: null,
            stanje: null
        }

        this.state = {zadatak: zadatak, sprintovi: [], stanje: []};
    }

    componentDidMount() {
       this.getZadatakById(this.props.match.params.id);
       this.getSprintovi();
       this.getStanja();
    }

    getZadatakById(zadatakId) {
        ZadatakAxios.get('/zadaci/' + zadatakId)
        .then(res => {
            // handle success
            console.log('ZADATAK:')
            console.log(res);
            this.setState({zadatakId: res.data.id, sprint:res.data.sprint, ime:res.data.ime,  zaduzeni:res.data.zaduzeni, bodovi:res.data.bodovi, stanje:res.data.stanje, pocetakZadatka:res.data.pocetakZadatka});
        })
        .catch(error => {
            // handle error
            console.log(error);
            alert('Error occured please try again!');
         });
    }

    async getSprintovi(){
        try{
            let result = await ZadatakAxios.get("/sprintovi");
            let sprintovi = result.data;
            this.setState({sprintovi: sprintovi});
        }catch(error){
            console.log(error);
            alert("Couldn't fetch");
        }
    }
    async getStanja(){
        try{
            let result = await ZadatakAxios.get("/stanja");
            let stanja = result.data;
            this.setState({stanja: stanja});
        }catch(error){
            console.log(error);
            alert("Couldn't fetch");
        }
    }
 

    onImeChange = event => {
        console.log(event.target.value);

        const { name, value } = event.target;
        console.log(name + ", " + value);

        this.setState((state, props) => ({
            
            ime: value
        }));
    }
    onZaduzeniChange = event => {
        console.log(event.target.value);

        const { name, value } = event.target;
        console.log(name + ", " + value);

        this.setState((state, props) => ({
            
            zaduzeni: value
        }));
    }

    onBodoviChange = event => {
        console.log(event.target.value);

        const { name, value } = event.target;
        console.log(name + ", " + value);

        this.setState((state, props) => ({
           
            bodovi: value
        }));
    }
    onSprintChange = event => {
        console.log('Vrednost selecta')
        console.log(event.target.value);
        const sprintId = event.target.value;
        let odabranSprint = this.state.sprintovi.find((sprint) => sprint.id == sprintId);

        const { name, value } = event.target;
        console.log(name + ", " + value);

        this.setState((state, props) => ({
            sprint: odabranSprint
        }));
        console.log('na kraju state sprinta');
        console.log(this.state)
    }
    onPocetakZadatkaChange = event => {
        console.log(event.target.value);

        const { name, value } = event.target;
        console.log(name + ", " + value);

        this.setState((state, props) => ({
            
            pocetakZadatka: value
        }));
    }
    edit() {
        var params = {
            'id': this.state.zadatakId,
            'sprint': this.state.sprint,
            'pocetakZadatka': this.state.pocetakZadatka,
            'stanje': this.state.stanje,
            'ime': this.state.ime,
            'zaduzeni': this.state.zaduzeni,
            'bodovi' : this.state.bodovi
        };

        ZadatakAxios.put('/zadaci/' + this.state.zadatakId, params)
        .then(res => {
            // handle success
            console.log(res);
            alert('Zadatak uspesno izmenjen!');
            this.props.history.push('/zadaci');
        })
        .catch(error => {
            // handle error
            console.log(error);
            alert('Error occured please try again!');
         });
    }

    render() {
        return (
            <div>
                <h1>Edit zadatak</h1>
                <Form>
                    <Form.Group>
                    
                    <Form.Label htmlFor="ime">Ime</Form.Label>
                    <Form.Control id="ime" name="ime" value={this.state.ime} onChange={(e) => this.onImeChange(e)}/>
                    <Form.Label htmlFor="zaduzeni">Zaduzeni</Form.Label>
                    <Form.Control id="zaduzeni" name="zaduzeni" value={this.state.zaduzeni} onChange={(e) => this.onZaduzeniChange(e)}/>
                    <Form.Label htmlFor="pocetakZadatka">Pocetak zadatka</Form.Label>
                    <Form.Control id="pocetakZadatka" name="pocetakZadatka"  value={this.state.pocetakZadatka} onChange={(e) => this.onPocetakZadatkaChange(e)}/>
                    <Form.Label htmlFor="bodovi">Bodovi</Form.Label>
                    <Form.Control id="bodovi" name="bodovi" type="number" value={this.state.bodovi} onChange={(e) => this.onBodoviChange(e)}/>
                    <Form.Label htmlFor="sprint">Sprint</Form.Label>
                    <Form.Control as="select" id="sprint" onChange={(e) => this.onSprintChange(e)}>
                        <option></option>
                        {
                            this.state.sprintovi.map((sprint) => {
                                return (
                                    <option key={sprint.id} value={sprint.id}>{sprint.ime}</option>
                                )
                            })
                        }
                    </Form.Control><br/>
                    <Button className="button button-navy" onClick={() => this.edit()}>Edit</Button>
                    </Form.Group>
                    </Form>
                
            </div>
        );
    }
}

export default EditZadatak;