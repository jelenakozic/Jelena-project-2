import React from "react";
import ReactDOM from "react-dom";
import { Route, Link, HashRouter as Router, Switch, Redirect } from "react-router-dom";
import Home from "./components/Home";
import AddZadatak from "./components/zadaci/AddZadatak";
import EditZadatak from "./components/zadaci/EditZadatak";
import Zadaci from "./components/zadaci/Zadaci";

import NotFound from "./components/NotFound";
import Login from './components/authentication/Login';


import { Navbar, Nav, Container, Button } from "react-bootstrap";
import {logout} from './services/auth';

class App extends React.Component {
    render() {
  
      let token = window.localStorage.getItem("token");
  
      if(token){
        return (
          <div>
            <Router>
              <Navbar bg="dark" variant="dark" expand fixed="top">
                <Navbar.Brand>
                  <Link to="/">JWD</Link>
                </Navbar.Brand>
                <Nav className="mr-auto">
                  <Nav.Link as={Link} to="/zadaci">
                    Zadaci
                  </Nav.Link>
                  
                </Nav>
                <Button onClick={()=>logout()}>Logout</Button>
              </Navbar>
    
              <br />
              <br />
              <br />
    
              <Container>
                <Switch>
                  <Route exact path="/" component={Home} />
                  <Route exact path="/login" render={()=><Redirect to="/" />} />
                  <Route exact path="/zadaci" component={Zadaci} />
                  <Route exact path="/zadaci/add" component={AddZadatak} />
                  <Route exact path="/zadaci/edit/:id" component={EditZadatak} />
                 
                  <Route component={NotFound} />
                </Switch>
              </Container>
            </Router>
          </div>
        );
      }else{
  
        return (
          <Container>
            <Router>
              <Switch>
                <Route exact path="/login" component={Login} />
                <Route render={()=><Redirect to="/login" />} />
              </Switch>
            </Router>
          </Container>
        );
  
      }
  
      
    }
  }
  
  ReactDOM.render(<App />, document.querySelector("#root"));
  