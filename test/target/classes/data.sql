

INSERT INTO korisnik (id, e_mail, korisnicko_ime, lozinka, ime, prezime, uloga)
              VALUES (1,'miroslav@maildrop.cc','miroslav','$2y$12$NH2KM2BJaBl.ik90Z1YqAOjoPgSd0ns/bF.7WedMxZ54OhWQNNnh6','Miroslav','Simic','ADMIN');
INSERT INTO korisnik (id, e_mail, korisnicko_ime, lozinka, ime, prezime, uloga)
              VALUES (2,'tamara@maildrop.cc','tamara','$2y$12$DRhCpltZygkA7EZ2WeWIbewWBjLE0KYiUO.tHDUaJNMpsHxXEw9Ky','Tamara','Milosavljevic','KORISNIK');
INSERT INTO korisnik (id, e_mail, korisnicko_ime, lozinka, ime, prezime, uloga)
              VALUES (3,'petar@maildrop.cc','petar','$2y$12$i6/mU4w0HhG8RQRXHjNCa.tG2OwGSVXb0GYUnf8MZUdeadE4voHbC','Petar','Jovic','KORISNIK');


INSERT INTO sprint (id, ime, ukupni_bodovi) VALUES (1, 'Sprint 1', 10);
INSERT INTO sprint (id, ime, ukupni_bodovi) VALUES (2, 'Sprint 2', 20);
INSERT INTO sprint (id, ime, ukupni_bodovi) VALUES (3, 'Sprint 3', 30);

INSERT INTO stanje (id, ime) VALUES (1, 'Pocetak');
INSERT INTO stanje (id, ime) VALUES (2, 'Sredina');
INSERT INTO stanje (id, ime) VALUES (3, 'Kraj');

INSERT INTO zadatak (id, ime, zaduzeni, bodovi, pocetak_zadatka, sprint_id, stanje_id) VALUES (1, 'JUnit testovi', 'Tim 1', 5, '2020-06-21 09:00', 1, 2);
INSERT INTO zadatak (id, ime, zaduzeni, bodovi, pocetak_zadatka, sprint_id, stanje_id) VALUES (2, 'Backend', 'Tim 2', 7, '2020-12-21 08:00', 2, 2);
INSERT INTO zadatak (id, ime, zaduzeni, bodovi, pocetak_zadatka, sprint_id, stanje_id) VALUES (3, 'Frontend', 'Tim 3', 8, '2020-10-25 08:30', 3, 3);
INSERT INTO zadatak (id, ime, zaduzeni, bodovi, pocetak_zadatka, sprint_id, stanje_id) VALUES (4, 'Transakcije', 'Tim 4', 6, '2020-09-27 20:00', 1, 3);
              
