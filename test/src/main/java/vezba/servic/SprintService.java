package vezba.servic;

import java.util.List;

import vezba.model.Sprint;

public interface SprintService {
	
	List<Sprint> findAll();
	
	Sprint findOne(Long id);
	
	Sprint update(Sprint sprint);

}
