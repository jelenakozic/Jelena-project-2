package vezba.servic;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.domain.Page;



import vezba.model.Zadatak;

public interface ZadatakService {

	 List<Zadatak> findAll();
	 
	 Zadatak findOne(Long id);
	 
	 Zadatak save(Zadatak zadatak);
	 
	 Zadatak update(Zadatak zadatak);
	 
	 Zadatak delete(Long id);
	 
	 Page<Zadatak> pretraga( Long sprintId, String ime, int pageNum);
}
