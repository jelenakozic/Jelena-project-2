package vezba.servic;

import java.util.List;

import vezba.model.Stanje;

public interface StanjeService {

	List<Stanje> findAll();
	
	Stanje findOne(Long id);
}
