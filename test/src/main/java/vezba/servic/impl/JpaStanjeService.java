package vezba.servic.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import vezba.model.Stanje;
import vezba.repository.StanjeRepository;
import vezba.servic.StanjeService;

@Service
public class JpaStanjeService implements StanjeService {

	@Autowired
	private StanjeRepository stanjeRepository;
	
	@Override
	public List<Stanje> findAll() {
		return stanjeRepository.findAll();
	}

	@Override
	public Stanje findOne(Long id) {
		
		return stanjeRepository.findOneById(id);
	}

}
