package vezba.servic.impl;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import vezba.model.Zadatak;
import vezba.repository.ZadatakRepository;
import vezba.servic.ZadatakService;

@Service
public class JpaZadatakService implements ZadatakService{

	 @Autowired
	    private ZadatakRepository zadatakRepository;

	@Override
	public List<Zadatak> findAll() {
		
		return zadatakRepository.findAll();
	}

	@Override
	public Zadatak findOne(Long id) {
		
		return zadatakRepository.findOneById(id);
	}

	@Override
	public Zadatak save(Zadatak zadatak) {
		
		return zadatakRepository.save(zadatak);
	}

	@Override
	public Zadatak update(Zadatak zadatak) {
		return zadatakRepository.save(zadatak);
	}

	@Override
	public Zadatak delete(Long id) {
		Zadatak zadatak = findOne(id);
        if(zadatak != null){
        	zadatak.getSprint().getZadaci().remove(zadatak);
        	zadatak.setSprint(null);
        	
        	zadatak.getStanje().getZadaci().remove(zadatak);
        	zadatak.setStanje(null);
        	
        	zadatak = zadatakRepository.save(zadatak);
            zadatakRepository.delete(zadatak);
            return zadatak;
        }
        return null;
	}

	@Override
	public Page<Zadatak> pretraga(Long sprintId, String ime, int pageNum) {
		return zadatakRepository.pretraga(sprintId,  ime, PageRequest.of(pageNum, 5));
	}
	 
	 
	 
}
