package vezba.servic.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import vezba.model.Sprint;
import vezba.repository.SprintRepository;
import vezba.servic.SprintService;

@Service
public class JpaSprintService implements SprintService {

	@Autowired
	private SprintRepository sprintRepository;
	
	@Override
	public List<Sprint> findAll() {
		return sprintRepository.findAll();
	}

	@Override
	public Sprint findOne(Long id) {
		
		return sprintRepository.findOneById(id);
	}

	@Override
	public Sprint update(Sprint sprint) {
		
		return sprintRepository.save(sprint);
	}

	
}
