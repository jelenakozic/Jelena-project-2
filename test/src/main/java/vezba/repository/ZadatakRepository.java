package vezba.repository;

import java.time.LocalDateTime;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;



import vezba.model.Zadatak;

@Repository
public interface ZadatakRepository extends JpaRepository<Zadatak,Long> {

	Zadatak findOneById(Long id);
	
	 @Query("SELECT z FROM Zadatak z WHERE "
	    		
	    		+ "(:sprintId IS NULL OR z.sprint.id = :sprintId) AND "
	    		
	    		+ "(:ime IS NULL OR z.ime like %:ime% )")
	    Page<Zadatak> pretraga( @Param("sprintId") Long sprintId,  @Param("ime") String ime, Pageable pageable);
}
