package vezba.web.dto;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class ZadatakDTO {

	private Long id;
	
	@NotNull(message = "Nije zadato trajanje filma.")
	@Size( max=40)
	private String ime;
	
	private String zaduzeni;
	
//	@Size(min=0, max=20)
	@Min(value=0)
	@Max(value=20)
	private int bodovi;
	
	private SprintDTO sprint;
	
	private StanjeDTO stanje;
	
	private String pocetakZadatka;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public String getZaduzeni() {
		return zaduzeni;
	}

	public void setZaduzeni(String zaduzeni) {
		this.zaduzeni = zaduzeni;
	}

	public int getBodovi() {
		return bodovi;
	}

	public void setBodovi(int bodovi) {
		this.bodovi = bodovi;
	}

	public SprintDTO getSprint() {
		return sprint;
	}

	public void setSprint(SprintDTO sprint) {
		this.sprint = sprint;
	}

	public StanjeDTO getStanje() {
		return stanje;
	}

	public void setStanje(StanjeDTO stanje) {
		this.stanje = stanje;
	}

	

	public String getPocetakZadatka() {
		return pocetakZadatka;
	}

	public void setPocetakZadatka(String pocetakZadatka) {
		this.pocetakZadatka = pocetakZadatka;
	}

	public ZadatakDTO() {
		super();
	}
	
	
	
	
  	
   
   
   
}