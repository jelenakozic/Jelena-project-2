package vezba.web.dto;

import java.util.HashSet;
import java.util.Set;

public class SprintDTO {

	private Long id;
	
	 private String ime;
	 
	 private int ukupniBodovi;
	 
	 private Set<ZadatakDTO> zadaci = new HashSet<ZadatakDTO>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public int getUkupniBodovi() {
		return ukupniBodovi;
	}

	public void setUkupniBodovi(int ukupniBodovi) {
		this.ukupniBodovi = ukupniBodovi;
	}

	public Set<ZadatakDTO> getZadaci() {
		return zadaci;
	}

	public void setZadaci(Set<ZadatakDTO> zadaci) {
		this.zadaci = zadaci;
	}

	public SprintDTO() {
		super();
	}
	 
	 
}
