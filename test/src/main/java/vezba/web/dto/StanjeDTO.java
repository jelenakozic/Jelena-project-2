package vezba.web.dto;

import java.util.HashSet;
import java.util.Set;

public class StanjeDTO {
	
	private Long id;
	
	private String ime;
	
	private Set<ZadatakDTO> zadaci = new HashSet<ZadatakDTO>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public Set<ZadatakDTO> getZadaci() {
		return zadaci;
	}

	public void setZadaci(Set<ZadatakDTO> zadaci) {
		this.zadaci = zadaci;
	}

	public StanjeDTO() {
		super();
	}
	
	

}
