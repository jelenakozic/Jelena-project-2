package vezba.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import vezba.model.Stanje;
import vezba.servic.StanjeService;
import vezba.servic.ZadatakService;
import vezba.support.StanjeDTOToStanje;
import vezba.support.StanjeToStanjeDTO;
import vezba.support.ZadatakToZadatakDTO;
import vezba.web.dto.StanjeDTO;

@RestController
@RequestMapping(value = "/api/stanja", produces = MediaType.APPLICATION_JSON_VALUE)
@Validated
public class StanjeController {

	@Autowired
    private StanjeService stanjeService;

    @Autowired
    private ZadatakService zadatakService;

    @Autowired
    private StanjeDTOToStanje toStanje;

    @Autowired
    private StanjeToStanjeDTO toStanjeDTO;

    @Autowired
    private ZadatakToZadatakDTO toZadatakDTO;
    
    @GetMapping
    public ResponseEntity<List<StanjeDTO>> getAll(
            @RequestParam(required=false) String ime,
            @RequestParam(required=false) Long zadatakId){

        List<Stanje> stanja = stanjeService.findAll();

        return new ResponseEntity<>(toStanjeDTO.convert(stanja), HttpStatus.OK);
    }

    
}
