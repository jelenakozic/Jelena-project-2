package vezba.web.controller;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import vezba.model.Sprint;
import vezba.model.Stanje;
import vezba.model.Zadatak;
import vezba.servic.SprintService;
import vezba.servic.StanjeService;
import vezba.servic.ZadatakService;
import vezba.support.ZadatakDTOToZadatak;
import vezba.support.ZadatakToZadatakDTO;
import vezba.web.dto.ZadatakDTO;

@RestController
@RequestMapping(value = "/api/zadaci", produces = MediaType.APPLICATION_JSON_VALUE)
public class ZadatakController {

	@Autowired
	private ZadatakService zadatakService;
	
	@Autowired
	private SprintService sprintService;
	
	@Autowired
	private StanjeService stanjeService;
	
	@Autowired
	private ZadatakToZadatakDTO zadatakToZadatakDTO;
	
	@Autowired
	private ZadatakDTOToZadatak zadatakDTOToZadatak;
	
	 @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	    public ResponseEntity<ZadatakDTO> create(@Valid @RequestBody ZadatakDTO zadatakDTO){
	        Zadatak zadatak = zadatakDTOToZadatak.convert(zadatakDTO);

	        if(zadatak.getSprint() == null) {
	            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
	        }
	        if(zadatak.getStanje() == null) {
	            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
	        }
	        
	        Zadatak sacuvanZadatak = zadatakService.save(zadatak);

	        return new ResponseEntity<>(zadatakToZadatakDTO.convert(sacuvanZadatak), HttpStatus.CREATED);
	    }
	 
	 @PutMapping(value= "/{id}/promenaStanja")
	 public ResponseEntity<ZadatakDTO> changeStanje(@PathVariable Long id){
		 
		 Zadatak zadatak = zadatakService.findOne(id);
		 List<Stanje> svaStanja = stanjeService.findAll();
		 
		String trenutnoImeStanja= zadatak.getStanje().getIme();
		if(trenutnoImeStanja.equals("Pocetak")) {
			String novoStanjeIme = "Sredina";
		
			for (Stanje stanje : svaStanja) {
				if (stanje.getIme().equals(novoStanjeIme)) {
					zadatak.setStanje(stanje);
				}
			}
		}
		
		if(trenutnoImeStanja.equals("Sredina")) {
			String novoStanjeIme = "Kraj";
			
			for(Stanje stanje: svaStanja) {
				if(stanje.getIme().equals(novoStanjeIme)) {
					zadatak.setStanje(stanje);
				}
			}
		}
//		if (trenutnoImeStanja.equals("Kraj")) {
//			String novoStanjeIme = "Kraj";
//			
//			for (Stanje stanje : svaStanja) {
//				if (stanje.getIme().equals(novoStanjeIme)) {
//					zadatak.setStanje(stanje);
//				}
//			}
//		}
		
		
		Zadatak sacuvanZadatak = zadatakService.update(zadatak);
		
		return new ResponseEntity<>(zadatakToZadatakDTO.convert(sacuvanZadatak),HttpStatus.OK);
	    
	 }

	 @PutMapping(value= "/{id}",consumes = MediaType.APPLICATION_JSON_VALUE)
	    public ResponseEntity<ZadatakDTO> update(@PathVariable Long id, @Valid @RequestBody ZadatakDTO zadatakDTO){

	        if(!id.equals(zadatakDTO.getId())) {
	            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
	        }

	        Zadatak zadatak = zadatakDTOToZadatak.convert(zadatakDTO);

	        if(zadatak.getStanje() == null) {
	            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
	        }
	        if(zadatak.getSprint() == null) {
	            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
	        }

	        Zadatak sacuvanZadatak = zadatakService.update(zadatak);
	        
	        
	        
	        // get sprint by zadatak.sprintId
	        // dobavili sprint
	        // get all zadaci gde je sprint id zadatak.sprintId (sprint.id)
	        // prodji for-om kroz dobavljenje zadatke i sumiraj njihove bodove u novu promenljivu
	        // sprint.setUkupniBodovi(sumirani bodovi)
	        // sprint.update(sprint)
	               
//	       Sprint trenutniSprint = sprintService.findOne(sacuvanZadatak.getSprint().getId()); 
	        
	       Sprint trenutniSprint = sacuvanZadatak.getSprint();

	       List<Zadatak> sviZadaci = trenutniSprint.getZadaci();
	       
	       int novoStanjeBodova = 0;
	       for (Zadatak zad : sviZadaci) {
	    	   novoStanjeBodova = novoStanjeBodova + zad.getBodovi();
	       }
	       
	       trenutniSprint.setUkupniBodovi(novoStanjeBodova);
	       
	       sprintService.update(trenutniSprint);
	       
	        
	       return new ResponseEntity<>(zadatakToZadatakDTO.convert(sacuvanZadatak),HttpStatus.OK);
	    }
	 
	 
	 @DeleteMapping("/{id}")
	    public ResponseEntity<Void> delete(@PathVariable Long id){
	        Zadatak obrisanZadatak = zadatakService.delete(id);

	        if(obrisanZadatak != null) {
	            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	        } else {
	            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	        }
	    }
	 
	 
	 
	 @GetMapping("/{id}")
	    public ResponseEntity<ZadatakDTO> getOne(@PathVariable Long id){
	        Zadatak zadatak = zadatakService.findOne(id);

	        if(zadatak != null) {
	            return new ResponseEntity<>(zadatakToZadatakDTO.convert(zadatak), HttpStatus.OK);
	        }else {
	            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	        }
	    }
	 @GetMapping
	    public ResponseEntity<List<ZadatakDTO>> getAll(
	    		@RequestParam(required=false) String pocetakZadatkaOdParametar,
	            @RequestParam(required=false) String pocetakZadatkaDoParametar,
	            @RequestParam(required=false) Long stanjeId,
	            @RequestParam(required=false) Long sprintId,
	            @RequestParam(required=false) String ime,
	            @RequestParam(required=false) String zaduzeni,
	            @RequestParam(required=false) Integer bodovi,
	            @RequestParam(defaultValue = "0") int pageNum){

	        
	       
	        
	        Page<Zadatak> zadaciPage = zadatakService.pretraga(sprintId,   ime, pageNum);
	        
	        HttpHeaders responseHeaders = new HttpHeaders();
	        responseHeaders.set("Total-Pages", zadaciPage.getTotalPages() + "");
	        
	        return new ResponseEntity<>(zadatakToZadatakDTO.convert(zadaciPage.getContent()), responseHeaders, HttpStatus.OK);
	    }

	    private LocalDateTime getLocalDateTime(String pocetakZadatka) throws DateTimeParseException {
	        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
	        LocalDate datum = LocalDate.parse(pocetakZadatka.substring(0, 10), formatter);
	        LocalTime vreme = LocalTime.parse(pocetakZadatka.substring(11), DateTimeFormatter.ofPattern("HH:mm"));
	        return LocalDateTime.of(datum, vreme);
	    }
	 
}
