package vezba.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import vezba.model.Sprint;
import vezba.servic.SprintService;
import vezba.servic.ZadatakService;
import vezba.support.SprintDTOToSprint;
import vezba.support.SprintToSprintDTO;
import vezba.support.ZadatakToZadatakDTO;
import vezba.web.dto.SprintDTO;

@RestController
@RequestMapping(value = "/api/sprintovi", produces = MediaType.APPLICATION_JSON_VALUE)
@Validated
public class SprintController {


	@Autowired
    private SprintService sprintService;

    @Autowired
    private ZadatakService zadatakService;

    @Autowired
    private SprintDTOToSprint toSprint;

    @Autowired
    private SprintToSprintDTO toSprintDTO;

    @Autowired
    private ZadatakToZadatakDTO toZadatakDTO;
    
    @GetMapping
    public ResponseEntity<List<SprintDTO>> getAll(
            @RequestParam(required=false) String ime,
            @RequestParam(required=false) Integer ukupniBodovi,
            @RequestParam(required=false) Long zadatakId){

        List<Sprint> sprint = sprintService.findAll();

        return new ResponseEntity<>(toSprintDTO.convert(sprint), HttpStatus.OK);
    }

    
}

