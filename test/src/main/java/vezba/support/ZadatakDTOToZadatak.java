package vezba.support;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import vezba.model.Zadatak;
import vezba.servic.SprintService;
import vezba.servic.StanjeService;
import vezba.servic.ZadatakService;
import vezba.web.dto.ZadatakDTO;

@Component
public class ZadatakDTOToZadatak implements Converter<ZadatakDTO, Zadatak> {


    @Autowired
    private ZadatakService zadatakService;

    @Autowired
    private StanjeService stanjeService;
    
    @Autowired
    private SprintService sprintService;

    @Override
    public Zadatak convert(ZadatakDTO dto) {
    	Zadatak zadatak;

        if(dto.getId() == null){
        	zadatak = new Zadatak();
        }else{
        	zadatak = zadatakService.findOne(dto.getId());
        }

        if(zadatak != null){
        	zadatak.setPocetakZadatka(getLocalDateTime(dto.getPocetakZadatka()));
        	zadatak.setSprint(sprintService.findOne(dto.getSprint().getId()));
        	zadatak.setStanje(stanjeService.findOne(dto.getStanje().getId()));
        	zadatak.setBodovi(dto.getBodovi());
        	zadatak.setIme(dto.getIme());
        	zadatak.setZaduzeni(dto.getZaduzeni());
        	
        }
        return zadatak;
    }

    private LocalDateTime getLocalDateTime(String pocetakZadatka) throws DateTimeParseException {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate datum = LocalDate.parse(pocetakZadatka.substring(0, 10), formatter);
        LocalTime vreme = LocalTime.parse(pocetakZadatka.substring(11), DateTimeFormatter.ofPattern("HH:mm"));
        return LocalDateTime.of(datum, vreme);
    }
}
