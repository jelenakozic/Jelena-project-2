package vezba.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;



import vezba.model.Sprint;
import vezba.web.dto.SprintDTO;

@Component
public class SprintToSprintDTO implements Converter <Sprint,SprintDTO> {

	@Override
	public SprintDTO convert(Sprint sprint) {
		
		SprintDTO dto = new SprintDTO();
		dto.setId(sprint.getId());
		dto.setIme(sprint.getIme());
		dto.setUkupniBodovi(sprint.getUkupniBodovi());
		
		return dto;
	}
	public List<SprintDTO> convert (List<Sprint> sprintovi) {
		List<SprintDTO> sprintoviDto = new ArrayList<>();

        for(Sprint sprint : sprintovi) {
        	sprintoviDto.add(convert(sprint));
        }

        return sprintoviDto;
	}

}
