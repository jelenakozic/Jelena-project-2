package vezba.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;



import vezba.model.Zadatak;
import vezba.web.dto.ZadatakDTO;

@Component
public class ZadatakToZadatakDTO implements Converter<Zadatak,ZadatakDTO>{

	 @Autowired
	    private SprintToSprintDTO sprintToSprintDTO;
	 
	 @Autowired
	    private StanjeToStanjeDTO stanjeToStanjeDTO;

	@Override
	public ZadatakDTO convert(Zadatak zadatak) {
		
		ZadatakDTO zadatakDTO= new ZadatakDTO();
		zadatakDTO.setId(zadatak.getId());
		zadatakDTO.setBodovi(zadatak.getBodovi());
		zadatakDTO.setIme(zadatak.getIme());
		zadatakDTO.setZaduzeni(zadatak.getZaduzeni());
		zadatakDTO.setPocetakZadatka(zadatak.getPocetakZadatka().toString());
		
		zadatakDTO.setStanje(stanjeToStanjeDTO.convert(zadatak.getStanje()));
		zadatakDTO.setSprint(sprintToSprintDTO.convert(zadatak.getSprint()));
		
		return zadatakDTO;
	}
	public List<ZadatakDTO> convert(List<Zadatak> zadaci){
        List<ZadatakDTO> zadaciDTO = new ArrayList<>();

        for(Zadatak zadatak : zadaci) {
        	zadaciDTO.add(convert(zadatak));
        }

        return zadaciDTO;
    }
}
