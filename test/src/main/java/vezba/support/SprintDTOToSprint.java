package vezba.support;

import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;



import vezba.model.Sprint;
import vezba.servic.SprintService;
import vezba.web.dto.SprintDTO;

@Component
public class SprintDTOToSprint implements Converter <SprintDTO, Sprint>{

	@Autowired
	private SprintService sprintService;
	
	@Override
	public Sprint convert(SprintDTO dto) {
		
		Sprint entity;
		if(dto.getId() == null) {
            entity = new Sprint();
        }else {
            entity = sprintService.findOne(dto.getId());
        }

        if(entity != null) {
            entity.setIme(dto.getIme());
            entity.setUkupniBodovi(dto.getUkupniBodovi());
            
            
        }

        return entity;
		
		
	}
}
