package vezba.support;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;


import vezba.model.Stanje;
import vezba.servic.StanjeService;

import vezba.web.dto.StanjeDTO;

@Component
public class StanjeDTOToStanje implements Converter <StanjeDTO, Stanje>{
	
	@Autowired
	private StanjeService stanjeService;
	
	@Override
	public Stanje convert(StanjeDTO dto) {
		
		Stanje entity;
		if(dto.getId() == null) {
            entity = new Stanje();
        }else {
            entity = stanjeService.findOne(dto.getId());
        }

        if(entity != null) {
            entity.setIme(dto.getIme());
           
            
            
        }

        return entity;
		
		
	}

}
